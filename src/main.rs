use argh::FromArgs;
use crossterm::{
    event::{ self, Event, KeyCode, KeyEvent },
};
use dirs;
use ratatui::{
    Terminal,
    prelude::*,
    widgets::{ Paragraph },
};
use serde::{ Serialize, Deserialize };
use serde_json;
use textwrap;
use time::{
    OffsetDateTime,
    format_description::well_known::Rfc2822,
};

use std::{
    fs::{ self, File },
    io::{ self, Write },
    mem,
    path::PathBuf,
};



#[derive(FromArgs)]
/// a to-do list app you didn't need
struct Args {
    /// print the path to the list, and exit
    #[argh(switch, short = 'p')]
    path: bool,

    /// start with a new list (warning: no turning back!)
    #[argh(switch)]
    reset: bool,

    /// open a specified list instead of the default one
    #[argh(positional, short = 'l')]
    list: Option<PathBuf>,
}



/// The mode of the app.
enum Mode {
    /// Main mode.
    Main,
    /// New mode, with the string buffer holding the content of the new list item.
    New(String),
    /// Quit mode.
    Quit,
}



/// The app.
struct App {
    /// The list.
    items: Vec<Item>,
    /// The mode.
    mode: Mode,
    /// The index of the focused list item.
    focus_idx: usize,
}



/// List item.
#[derive(Serialize, Deserialize)]
struct Item {
    /// Date on which this item was checked.
    ///
    /// If this item is unchecked, this is `None`.  Otherwise, it is
    /// `Some(date)` with some date `date`.
    check_date: Option<OffsetDateTime>,
    /// Content of the list item.
    string: String,
    /// Date on which this item was added.
    add_date: OffsetDateTime,
}



/// Convenient function that creates a string filled with `x` spaces.
///
/// Used for padding.
fn spaces(x: usize) -> String {
    std::iter::repeat(' ').take(x).collect()
}



/// Render the UI.
fn render(f: &mut Frame, app: &App) {
    let frame_width: usize = f.size().width as usize;

    // Word wrap width.
    let wordwrap_width: usize = (frame_width * 4 / 5).min(80);

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Min(1),
            Constraint::Length(1),
            Constraint::Length(1),
        ])
        .split(f.size());

    let mut list_text: Vec<Line> = vec![];
    let wordwrap_content_width: usize = wordwrap_width - 4;

    for (k, item) in app.items.iter().enumerate() {
        let checked = item.check_date.is_some();
        let focused = k == app.focus_idx;

        let wrapped_lines = textwrap::wrap(&item.string, wordwrap_content_width);
        for (j, wrapped_line) in wrapped_lines.iter().enumerate() {
            let prefix: Span =
                if j == 0 {
                    match checked {
                        true => format!(" *  ").dark_gray().crossed_out(),
                        false => format!(" *  ").into(),
                    }
                }
                else {
                    match checked {
                        true => format!("    ").dark_gray(),
                        false => format!("    ").into(),
                    }
                };

            let mut wrapped_span: Span = wrapped_line.to_owned().into();
            if checked {
                wrapped_span = wrapped_span.dark_gray().crossed_out();
            }

            let presuffix_len: usize = prefix.content.len() + wrapped_span.content.len();
            let date: Option<Span> =
                if j == wrapped_lines.len() - 1 {
                    Some(match item.check_date {
                        Some(check_date) =>
                            format!("(Checked: {})", check_date.format(&Rfc2822).unwrap())
                                .italic(),

                        None =>
                            format!("(Added: {})", item.add_date.format(&Rfc2822).unwrap())
                                .dark_gray().italic(),
                    })
                }
                else { None };

            let mut spans: Vec<Span> = vec![
                prefix,
                wrapped_span,
            ];

            if focused {
                spans = spans.into_iter()
                    .map(|span| span.reversed())
                    .collect();
            }

            if let Some(date) = date {
                if presuffix_len + date.content.len() > frame_width {
                    list_text.push(spans.into());
                    list_text.push(vec![
                        spaces(frame_width.checked_sub(date.content.len()).unwrap_or(0)).into(),
                        date,
                    ].into());
                }
                else {
                    spans.extend([
                        spaces(frame_width.checked_sub(presuffix_len + date.content.len()).unwrap_or(0)).into(),
                        date,
                    ]);
                    list_text.push(spans.into());
                }
            }
            else {
                list_text.push(spans.into());
            }
        }
    }

    if let Mode::New(string) = &app.mode {
        let prompted = format!("{}_", string);
        let wrapped_lines = textwrap::wrap(&prompted, wordwrap_content_width);
        for (j, wrapped_line) in wrapped_lines.into_iter().enumerate() {
            let prefix: Span =
                match j == 0 {
                    true  => format!(" *  "),
                    false => format!("    "),
                }.into();

            list_text.push(vec![ prefix, wrapped_line.into_owned().into() ].into());
        }
    }

    let list = Paragraph::new(list_text);
    f.render_widget(list, chunks[0]);

    let mut help_text = format!("{}", match &app.mode {
        Mode::Main   => " main mode    [j/k] move down/up    [n] new item    [c] check item    [s] sort items    [q] quit",
        Mode::New(_) => " new mode    [ascii] push char    [bksp] pop char    [enter] add item    [esc] back",
        Mode::Quit   => " quit mode    [y/n] save changes?    [esc] back",
    });
    help_text = format!(
        "{}{}",
        help_text,
        spaces(frame_width.checked_sub(help_text.len()).unwrap_or(0))
    );

    let help = Paragraph::new(help_text.bold().reversed());
    f.render_widget(help, chunks[1]);
}



enum MaybeExit {
    No,
    ExitAndSave,
    ExitButDontSave,
}



fn process_key_press(key: KeyEvent, app: &mut App, changed: &mut bool) -> MaybeExit {
    let mut exit_and_save_changes = MaybeExit::No;

    match &mut app.mode {
        Mode::Main => {
            match key.code {
                KeyCode::Char('j') => {
                    if app.focus_idx < (app.items.len() - 1) {
                        app.focus_idx += 1;
                    }
                },

                KeyCode::Char('k') => {
                    if app.focus_idx > 0 {
                        app.focus_idx -= 1;
                    }
                },

                KeyCode::Char('c') => {
                    if app.items[app.focus_idx].check_date.is_none() {
                        app.items[app.focus_idx].check_date = Some(
                            OffsetDateTime::now_local()
                                .unwrap_or_else(|_| OffsetDateTime::now_utc())
                        );
                        *changed = true;
                    }
                },
                KeyCode::Char('n') => {
                    app.mode = Mode::New(String::new());
                },

                KeyCode::Char('q') => {
                    if *changed {
                        app.mode = Mode::Quit;
                    }
                    else {
                        exit_and_save_changes = MaybeExit::ExitButDontSave;
                    }
                },

                _ => {},
            }
        },

        Mode::New(x) => {
            match key.code {
                KeyCode::Char(ch) => {
                    x.push(ch);
                },

                KeyCode::Backspace => {
                    x.pop();
                },

                KeyCode::Enter => {
                    let new_mode = Mode::Main;
                    let x = match mem::replace(&mut app.mode, new_mode) {
                        Mode::New(x) => x,
                        _ => panic!(),
                    };
                    app.items.push(Item {
                        check_date: None,
                        string: x,
                        add_date: OffsetDateTime::now_local()
                            .unwrap_or_else(|_| OffsetDateTime::now_utc())
                    });
                    *changed = true;
                },

                KeyCode::Esc => {
                    app.mode = Mode::Main;
                },

                _ => {},
            }
        },

        Mode::Quit => {
            match key.code {
                KeyCode::Char('y') => {
                    exit_and_save_changes = MaybeExit::ExitAndSave;
                },

                KeyCode::Char('n') => {
                    exit_and_save_changes = MaybeExit::ExitButDontSave;
                },

                KeyCode::Esc => {
                    app.mode = Mode::Main;
                },

                _ => {},
            }
        },
    }

    exit_and_save_changes
}



fn main() {
    let args: Args = argh::from_env();

    //  Get the path to to-do list
    let list_path: PathBuf = args.list.unwrap_or_else(|| {
        let mut dir: PathBuf = dirs::config_dir()
            .expect("can't get config directory");
        dir.push("agl.json");
        dir
    });

    if args.path {
        println!("{}", list_path.display());
        return;
    }

    let items: Vec<Item> =
        match args.reset {
            true => vec![],
            false => {
                //  Empty list if doesn't exist
                if !list_path.exists() {
                    vec![] 
                }
                else {
                    let list_json: String = fs::read_to_string(&list_path).expect("can't read list");
                    serde_json::from_str(&list_json).expect("deserialization error")
                }
            },
        };

    //  Initialize app 
    let mut app = App {
        items,
        mode: Mode::Main,
        focus_idx: 0,
    };

    //  Terminal stuff
    crossterm::terminal::enable_raw_mode().unwrap();
    crossterm::execute!(io::stderr(), crossterm::terminal::EnterAlternateScreen).unwrap();

    //  Initialize terminal backend
    let mut terminal = Terminal::new(CrosstermBackend::new(io::stderr())).unwrap();

    //  Was there a change?
    let mut changed = false;

    //  UI loop
    let save_changes: bool = loop {
        //  Render UI
        terminal.draw(|f| render(f, &app)).unwrap();

        //  React to event
        if let Ok(Event::Key(key)) = event::read() {
            if key.kind == event::KeyEventKind::Release {
                continue;
            }

            match process_key_press(key, &mut app, &mut changed) {
                MaybeExit::No => {},
                MaybeExit::ExitAndSave => { break true; },
                MaybeExit::ExitButDontSave => { break false; },
            }
        }
    };

    //  Clean up terminal
    crossterm::execute!(io::stderr(), crossterm::terminal::LeaveAlternateScreen).unwrap();
    crossterm::terminal::disable_raw_mode().unwrap();

    //  Rewrite list to file
    if save_changes {
        let mut list_file = File::create(&list_path).expect("can't create list");
        writeln!(list_file, "{}", serde_json::to_string(&app.items).expect("serialization error")).expect("write error");
    }
}
